//
//  PreguntasHeader.swift
//  FisioApp
//
//  Created by Fernando Reyes Bejarano on 4/25/19.
//

import UIKit

class PreguntasController: UIViewController {
    
    //MARK:- Preguntas
    //https://github.com/mukeshthawani/FAQView
    
    let items = [FAQItem(question: "¿LAS CONTRACTURAS SE QUITAN O SE QUEDAN PARA SIEMPRE?", answer: "¡¡Claro que se quitan!! Las contracturas musculares aparecen por un aumento de contracción de sus fibras. Este aumento se puede reducir hasta llegar a desaparecer. \n\nSin embargo, es muy frecuente que se vuelvan a generar en el mismo lugar después de un tiempo. Esto sucede porque cada cuerpo, por su postura y su actividad, tiende a contraer un grupo muscular con mayor intensidad. En cualquier caso, tranquilos, esto también se puede mejorar con un buen tratamiento de conciencia corporal gracias al trabajo de los Fisioterapeutas."),
                 FAQItem(question: "¿ME PONGO FRÍO O CALOR?", answer: "En un proceso agudo de inflamación o dolor (esguinces, tendinitis, contracturas, contusiones, etc…), es decir, en las primeras 48 horas, es recomendable aplicar frío. Pasado ese tiempo, la inflamación empieza a “endurecerse” y es mejor poner calor para que se reabsorba. Sin embargo, por mi experiencia, recomendaría que escuchases a tu cuerpo, porque no todo el mundo responde de la misma manera. \n\nDel mismo modo, en la musculatura de la espalda, y muy especialmente, en el cuello y en la zona lumbar, es preferible no aplicar frío, ni siquiera en ese período agudo. Mejor no aplicar nada, y en unos días, poner calor; esto es así porque la sensación de frío es tan desagradable que provocaría más tensión muscular y empeoraría el dolor. \n\nTambién es muy recomendable los baños de contraste (3′ calor-1′ frío)."),
                 FAQItem(question: "¿PUEDO IR A FISIOTERAPIA SI ESTOY EMBARAZADA?", answer: "Sí, tratamos muchos problemas relacionados con el embarazo, pero por favor informa al fisioterapeuta antes del tratamiento para que tenga en cuenta."),
                 FAQItem(question: "¿ES BUENO TRATARSE CUANDO UN DOLOR ESTÁ MUY AGUDO?", answer: "Cuando el dolor es demasiado intenso o agudo, es habitual que el médico recomiende no empezar un tratamiento de fisioterapia; o incluso nosotros mismos, pensar que no nos va a venir bien. En cierto modo, es así. Cuando el cuerpo está a la defensiva, ciertos tratamientos le pueden resultar agresivos. Pero esto debe matizarse: \n\nSÍ se puede tratar una patología en su primer estadío, teniendo en cuenta que el tratamiento a realizar será menos invasivo, más enfocado a reducir la inflamación y el dolor según el caso. Simplemente, utilizaremos otras herramientas de trabajo. \n\nEn definitiva, mandar señales de normalidad a la zona problemática seguro que ayuda a la recuperación de la misma. No tengáis miedo si estáis en manos de profesionales; ellos saben lo que pueden y no pueden hacer."),
                 FAQItem(question: "¿POR QUÉ DEBEMOS ACUDIR A UN FISIOTERAPEUTA Y NO A OTROS PROFESIONALES?", answer: "Los masajistas, quiromasajistas y otros profesionales carecen de titulación oficial cualificada y no pueden aplicar tratamientos físicos propios de la Fisioterapia. \n\n“Fisioterapia” NO es un término genérico y ningún profesional que no sea fisioterapeuta puede aplicar ninguna rama de la Fisioterapia. Técnicas como el masaje con fines terapéuticos, curativos y sanitarios, la osteopatía, los vendajes funcionales o el drenaje linfático deben ser realizados únicamente por los profesionales de la Fisioterapia. \n\nLa Fisioterapia es salud y un fisioterapeuta el único profesional formado, cualificado y legalmente capacitado para atenderle mediante estas técnicas."),
                 FAQItem(question: "¿CUÁL ES LA MEJOR ALMOHADA?", answer: "Es imposible determinar la almohada estándar para toda la población. Entre nosotros hay diferencias anatómicas, por las que el cuello, la cabeza y los hombros se posicionan de distintas maneras. Por esta razón, es impensable que a todos nos venga bien la misma. \n\nEn aquellos individuos en los que observamos la cabeza muy adelantada respecto de los hombros, es más conveniente una almohada un poco más alta. En la posición inversa, con la cabeza más posteriorizada, elegiríamos una más baja. \n\nLlegamos a la conclusión de que la mejor almohada es aquella que cubre totalmente cabeza y cuello, y salvo esta observación, haced caso a vuestras sensaciones. Aquella almohada con la que os sintáis a gusto os permitirá un mejor descanso durante la noche, y eso es lo más importante."),
                 FAQItem(question: "¿CÓMO DEBE SER EL COLCHÓN?", answer: "La conclusión final es la misma que en la pregunta anterior: es difícil dar por válido un solo colchón para todo el mundo. Aun así, sí que quiero concretar algunos aspectos que nos pueden orientar a la hora de elegir uno u otro. \n\nSujetos con una musculatura muy dura, poco flexibles y que tienden a la rigidez descansarán mejor con un colchón ligeramente más blando, que les induzca una relajación corporal. Uno muy duro provocaría en ellos mucha tensión durante la noche. \n\nAsí mismo, sujetos con más inestabilidad articular, laxos y con una musculatura más blanda, necesitan un colchón más firme, que aumenten ligeramente el tono del cuerpo."),
                 FAQItem(question: "¿SI ME ACABO DE HACER UN ESGUINCE DE TOBILLO O RODILLA PUEDO RECIBIR TRATAMIENTO DE FISIOTERAPIA?", answer: "Si, En función del grado podemos actuar desde la fisioterapia, es importante realizar una buena valoración y en este caso podremos proceder al tratamiento."),
                 FAQItem(question: "¿CADA CUÁNTO TIEMPO ES BUENO ACUDIR AL FISIOTERAPEUTA?", answer: "Este es un dato meramente orientativo y flexible en función del individuo y la patología, pero, en líneas generales, es conveniente recibir un tratamiento de fisioterapia una vez al mes. Así, eliminaremos las tensiones musculares que se hayan producido y evitaremos que deriven en patología, pudiendo realizar nuestras actividades diarias con normalidad y son dolor."),
                 FAQItem(question: "¿ES NORMAL QUE ME DUELA MÁS DESPUÉS DEL TRATAMIENTO?", answer: "Esta reacción del cuerpo asusta un poco cuando se siente, pero es algo que esperamos los fisioterapeutas tras hacer un tratamiento. Estimulamos el cuerpo, con el objetivo de que empiece a trabajar para encontrar la manera de solucionar el problema. Y esto supone un esfuerzo, que aparece en forma de sensación de agujetas, dolor diferente… sensaciones que suelen desaparecer en 48 horas como mucho, para dejar paso a la mejoría.")]
    
    
    
    // MARK:- UIViewController
    
    override func viewDidLoad() {
        let faqView : FAQView = FAQView(frame: view.frame, title: "Top Queries", items: items)
        
        // Question text color
        faqView.questionTextColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1)
        
        // Answer text color
        faqView.answerTextColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1)
        
        // View background color
        faqView.viewBackgroundColor = UIColor.init(red: 3/255, green: 34/255, blue: 48/255, alpha: 1)
        
        faqView.cellBackgroundColor = UIColor.init(red: 31/255, green: 70/255, blue: 89/255, alpha: 1)
        
        faqView.separatorColor = UIColor.init(red: 51/255, green: 104/255, blue: 118/255, alpha: 1)
        
        view.addSubview(faqView)
        
//        faqView.translatesAutoresizingMaskIntoConstraints = false
//        let margins = view.layoutMarginsGuide
//        let tableViewTrailing = faqView.leadingAnchor.constraint(equalTo: margins.leadingAnchor)
//        let tableViewLeading = faqView.trailingAnchor.constraint(equalTo: margins.trailingAnchor)
//        let guide = view.safeAreaLayoutGuide
//        let tableViewTop = faqView.topAnchor.constraint(equalToSystemSpacingBelow: guide.topAnchor, multiplier: 1.0)
//        let tableViewBottom = guide.bottomAnchor.constraint(equalToSystemSpacingBelow: faqView.bottomAnchor, multiplier: 0.0)
//        NSLayoutConstraint.activate([tableViewTrailing, tableViewLeading, tableViewTop, tableViewBottom])
        
//        let tableViewTrailing = NSLayoutConstraint(item: faqView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailingMargin, multiplier: 1, constant: 0)
//        let tableViewLeading = NSLayoutConstraint(item: faqView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leadingMargin, multiplier: 1, constant: 0)
//        let tableViewTop = NSLayoutConstraint(item: faqView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .topMargin, multiplier: 1, constant: 20)
//        let tableViewBottom = NSLayoutConstraint(item: faqView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottomMargin, multiplier: 1, constant: 0)
//        NSLayoutConstraint.activate([tableViewTrailing, tableViewLeading, tableViewTop, tableViewBottom])
        
//        let tableViewTrailing = NSLayoutConstraint(item: faqView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailingMargin, multiplier: 1, constant: 0)
//        let tableViewLeading = NSLayoutConstraint(item: faqView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leadingMargin, multiplier: 1, constant: 0)
//        let guide = view.safeAreaLayoutGuide
//        let tableViewTop = faqView.topAnchor.constraint(equalToSystemSpacingBelow: guide.topAnchor, multiplier: 1.0)
//        let tableViewBottom = guide.bottomAnchor.constraint(equalToSystemSpacingBelow: faqView.bottomAnchor, multiplier: 1.0)
//        NSLayoutConstraint.activate([tableViewTrailing, tableViewLeading, tableViewTop, tableViewBottom])
        
        faqView.translatesAutoresizingMaskIntoConstraints = false
        let tableViewTrailing = NSLayoutConstraint(item: faqView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
        let tableViewLeading = NSLayoutConstraint(item: faqView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
        let tableViewTop = NSLayoutConstraint(item: faqView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0)
        let tableViewBottom = NSLayoutConstraint(item: faqView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([tableViewTrailing, tableViewLeading, tableViewTop, tableViewBottom])
        
    }

}
