//
//  HistorialEjerciciosDAO.swift
//  FisioApp
//
//  Created by CHRISTIAN GUSTAVO CHAVARRO ESPEJO on 4/24/19.
//


import Foundation
import FirebaseFirestore
import CoreData

class HistorialEjerciciosDAO {
    
    //MARK:- Atributos.
    
    let db : Firestore = Firestore.firestore()
    
    var ejercicios = [EjercicioEntityDTO]()
    
    //MARK:- Constructores
    
    init(){
      //  actualizarFirebase()
    }
    
    //MARK:- Funciones.
    
  
    
    
    
    private func cargarEjercicios() {
        DispatchQueue.main.async {
            print("va a cargar los ejercicios")
            let dao = EjercicioEntityDAO()
            dao.getEjerciciosParaHoy{
                ejercicios in
                self.ejercicios = ejercicios
                 print("Cargo" + String(self.ejercicios.count) +  "Ejercicios en el asincrono")
            }
            
          print("Cargo" + String(self.ejercicios.count) +  "Ejercicios")
            
        }
    }
    
    func getHistorialesEjercicio(completed: @escaping ([HistorialEjerciciosDTO]) -> Void) -> Void  {
        
        cargarEjercicios()
        
        ejercicios.sort(by: {$0.creacion>$1.creacion})
    var historialesEjercicios=[HistorialEjerciciosDTO]()
        var espacio = HistorialEjerciciosDTO(completitud: false, creacion: Date.init(timeIntervalSinceReferenceDate: 14000), ejercicios: ejercicios)
        historialesEjercicios.append(espacio)
        
        
        var g=0
        
        for ejercicio in ejercicios
        {
            print("Entra a la lista de ejercicios")
            
            let dateEjercicio = ejercicio.creacion
            let format = DateFormatter()
            format.dateFormat = "dd.MM.yy"
            let formattedDateEjercicio1 = format.string(from: dateEjercicio)
            print(formattedDateEjercicio1 + "Primer date")
            
            let date = espacio.creacion
            let format1 = DateFormatter()
            format.dateFormat = "dd.MM.yy"
            let formattedDate1 = format1.string(from: date)
            print(formattedDate1 + "Date Creacion")
            
            if(formattedDate1==formattedDateEjercicio1)
            {
                historialesEjercicios[0].ejercicios.append(ejercicio)
            }
            else
            {
                let  EjerciciosPOJO = [ejercicio]
                
                 espacio = HistorialEjerciciosDTO(completitud: false, creacion: date, ejercicios: EjerciciosPOJO)
                g+=1
            }
        }
       // historialesEjercicios.removeFirst(1)
        
        historialesEjercicios.sort(by: {$0.creacion>$1.creacion})
        
        for histo in historialesEjercicios{
            histo.calcularCompleto2()
            print("Calcula completitud")
        }
        
        
        completed(historialesEjercicios)
        
        
    }
    
    
   
    
    func entityToDTO(ejercicioEntity ejerAlmacenado : NSManagedObject) -> EjercicioEntityDTO{
        var imagenEjer : UIImage?
        if let imagen = UIImage(named: ejerAlmacenado.value(forKey: "nombre") as! String) {
            imagenEjer = imagen
            print("La imagen existe: \(ejerAlmacenado.value(forKey: "nombre") as! String)")
        } else {
            imagenEjer = UIImage(named: "Sentadillas")
            print("La imagen no existe: \(ejerAlmacenado.value(forKey: "nombre") as! String)")
        }
        return EjercicioEntityDTO(nombre: ejerAlmacenado.value(forKey: "nombre") as! String, tipoEjercicio: ejerAlmacenado.value(forKey: "tipoEjercicio") as! String, imagen: imagenEjer,
                                  completitud: ejerAlmacenado.value(forKey: "completitud") as! Bool, creacion: ejerAlmacenado.value(forKey: "creacion") as! Date, descripcion: ejerAlmacenado.value(forKey: "descripcion") as! String, duracion: ejerAlmacenado.value(forKey: "duracion") as! Int, repeticiones: ejerAlmacenado.value(forKey: "repeticiones") as! Int, videoId: ejerAlmacenado.value(forKey: "videoId") as! String, documentId : ejerAlmacenado.value(forKey: "documentId") as! String, cantidadRepeticiones : ejerAlmacenado.value(forKey: "cantidadRepeticiones") as! Int)
    }
}
