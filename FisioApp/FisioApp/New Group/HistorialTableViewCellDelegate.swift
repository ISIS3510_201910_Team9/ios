//
//  HistorialTableViewCellDelegate.swift
//  FisioApp
//
//  Created by laura daniela parra lorza on 4/24/19.
//

import Foundation
protocol HistorialTableViewCellDelegate : AnyObject {
    func fuePresionadoDia(_ ejercicioTableViewCell : HistorialTableViewCell)

}
