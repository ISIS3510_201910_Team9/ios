//
//  HistorialTableViewController.swift
//  FisioApp
//
//  Created by FERNANDO REYES BEJARANO on 3/26/19.
//

import UIKit

class HistorialTableViewController: UITableViewController {
    
    let shapeLayer = CAShapeLayer()
    var historiales = [HistorialEjerciciosDTO]()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
         self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        cargarHistoriales()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return historiales.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HistorialTableViewCell", for: indexPath) as? HistorialTableViewCell else {
            fatalError("No sé por qué cualquier ser humano usaría esto.")
        }
        
        let ejercicio = historiales[indexPath.row]

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.locale = Locale(identifier: "es_CO")
        cell.labelDia.text = "Ejercicios del \(dateFormatter.string(from: ejercicio.creacion))"
        

        let center = cell.vistaAvance.center
        //TRack layer

        let label1 = UILabel(frame: CGRect.init(x: 53, y: 65, width: 280, height: 15))
            label1.text = "10 de 15"
               cell.vistaAvance.addSubview(label1)
        

            let circularPath2=UIBezierPath(arcCenter: center , radius: 62, startAngle: -CGFloat.pi/2, endAngle: CGFloat.pi, clockwise: true)
                shapeLayer.path=circularPath2.cgPath
                shapeLayer.lineCap=CAShapeLayerLineCap.round
                shapeLayer.strokeColor=UIColor.white.cgColor
                shapeLayer.fillColor=UIColor.clear.cgColor
        
                shapeLayer.lineWidth=7
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue=1
        basicAnimation.duration=1
       basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion=false
        
        tableView.layer.addSublayer(shapeLayer)
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    private func cargarHistoriales()
    {
        
        DispatchQueue.main.async {
            let dao = HistorialEjerciciosDAO()
            dao.getHistorialesEjercicio {
                ejercicios in
                self.historiales = ejercicios
              self.tableView.reloadData()
            }
        }
    }
    


}
