//
//  HistorialEjerciciosDTO.swift
//  FisioApp
//
//  Created by CHRISTIAN GUSTAVO CHAVARRO ESPEJO on 4/24/19.
//

import UIKit

class HistorialEjerciciosDTO {
    
    //MARK:- Constantes
    
    var ejercicios : [EjercicioEntityDTO] = []
    var creacion : Date
    var completitud : Bool
    
    
    
    
    //MARK:- Constructores
    
    init(completitud: Bool, creacion : Date, ejercicios: [EjercicioEntityDTO] ){
        self.ejercicios=ejercicios
        self.completitud = completitud
        self.creacion = creacion
        print("creo un dia de historiales")
        print(creacion.description + "Fecha del dia creado")
        
        
        
        
    }
    
    
    func calcularCompleto() ->[Int]
    {
    completitud=true
        var x=0
    for ejer in ejercicios
    {
     if(ejer.completitud==false)
     {
        completitud=false
        break
    }
     else{
        x+=1
        }
    }
        
        return [ejercicios.count, x]
    }
    
    
    func calcularCompleto2()
    {
        completitud=true
        
        for ejer in ejercicios
        {
            if(ejer.completitud==false)
            {
                completitud=false
                break
            }
        }
    }
}
