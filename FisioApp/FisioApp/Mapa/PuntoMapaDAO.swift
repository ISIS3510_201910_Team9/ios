//
//  PuntoMapaDAO.swift
//  FisioApp
//
//  Created by Christian Chavarro Espejo on 4/23/19.
//


import Foundation
import FirebaseFirestore
import CoreData

class PuntoMapaDAO {
    
    //MARK:- Atributos.
    
    let db : Firestore = Firestore.firestore()
    
    //MARK:- Constructores
    
    init(){
       // actualizarFirebase()
    }
    
    //MARK:- Funciones.

    
    
    func getPuntosCentrosMedicos(completed: @escaping([PuntoMapaDTO])->Void) -> Void{
        NetworkManager.isReachable {
            _ in
            self.getPuntosFirestore(completed: completed)
        }
        NetworkManager.isUnreachable {
            _ in
            self.getPuntosLocal(completed: completed)
        }
        
    }
    
    
     private func getPuntosFirestore(completed: @escaping ([PuntoMapaDTO]) -> Void) {
        
        db.collection("ColeccionLocaciones").document("Bogota").collection("Sitios").getDocuments {
            (snapshot, err) in
           // var ejercicios : [EjercicioEntityDTO] = []
            var puntos :[PuntoMapaDTO] = []
            var latitud : Double
            var longitud: Double
            var mensaje: String
            var nombreSitio: String
            var documentId : String
            var value : NSDictionary
          //  var numeroContacto: Int
          
            for document in snapshot!.documents {
                value = document.data() as NSDictionary
                
                nombreSitio = value["nombre"] as? String ?? "Queso"
                latitud = value["latitud"] as? Double ?? 4.00
                longitud = value["longitud"] as? Double ?? 72.00
                mensaje = value["mensaje"] as? String ?? ""
          //      numeroContacto = value["numeroContacto"] as? Int ?? 3192684086
                
                documentId = document.documentID
            
                
                puntos.append(PuntoMapaDTO(mensaje: mensaje, nombreSitio: nombreSitio, latitud: latitud, longitud: longitud, documentId: documentId))
                
                
         //       puntos.append(PuntoMapaDTO(mensaje: mensaje, nombreSitio: nombreSitio, latitud: latitud, longitud: longitud, documentId: documentId, numeroContacto: numeroContacto))
            }
       //     self.guardarPuntosLocal(puntos: puntos)
            completed(puntos)
        }
        
    }
    
    
    
    
   
    
    private func guardarPuntosLocal(ejercicios : [PuntoMapaDTO]){
        // https://medium.com/xcblog/core-data-with-swift-4-for-beginners-1fc067cca707
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        var entity : NSEntityDescription?
        var newEjer : NSManagedObject
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Punto")
        request.returnsObjectsAsFaults = false
        var result : [Any]
        for ejer in ejercicios {
            request.predicate = NSPredicate(format: "documentId = %@", ejer.documentId)
            do {
                result = try context.fetch(request)
                if result.count == 0 {
                    entity = NSEntityDescription.entity(forEntityName: "Ejercicio", in: context)
                    newEjer = NSManagedObject(entity: entity!, insertInto: context)
                    newEjer.setValue(ejer.mensaje, forKey: "mensaje")
                    newEjer.setValue(ejer.latitud, forKey: "latitud")
                    newEjer.setValue(ejer.longitud, forKey: "longitud")
                    newEjer.setValue(ejer.numeroContacto, forKey: "numeroContacto")
                    newEjer.setValue(ejer.nombreSitio, forKey: "nombreSitio")

                  
                    newEjer.setValue(ejer.documentId, forKey: "documentId")
                   
                }
            } catch {
                print("Failed")
            }
        }
        do {
            try context.save()
        } catch {
            print("Failed saving to CoreData")
        }
    }
    
    private func getPuntosLocal(completed: @escaping ([PuntoMapaDTO]) -> Void) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Punto")
        request.returnsObjectsAsFaults = false
        var result : [NSManagedObject]
        var ejercicios : [PuntoMapaDTO] = []
        
        do {
            result = try context.fetch(request) as! [NSManagedObject]
            for ejerAlmacenado in result {
                ejercicios.append(entityToDTO(puntoEntity: ejerAlmacenado))
            }
            completed(ejercicios)
        } catch {
            
            print("Failed")
        }
    }
    
//    public func actualizarEjercicio(_ ejercicio : EjercicioEntityDTO){
//        actualizarEjercicioFirestore(ejercicio)
//        actualizarEjercicioLocal(ejercicio)
//    }
//
//    private func actualizarEjercicioFirestore(_ ejercicio : EjercicioEntityDTO){
//        NetworkManager.isReachable{
//            _ in
//            self.db.collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(ejercicio.documentId).setData([
//                "completitud" : ejercicio.completitud
//            ], merge: true) { err in
//                if let err = err {
//                    print("Error writing document: \(err)")
//                } else {
//                    print("Document successfully written!")
//                }
//            }
//        }
//        NetworkManager.isUnreachable{
//            _ in
//            // https://medium.com/xcblog/core-data-with-swift-4-for-beginners-1fc067cca707
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            let context = appDelegate.persistentContainer.viewContext
//            var ejerAlmacenado : NSManagedObject = NSManagedObject()
//            var request = NSFetchRequest<NSFetchRequestResult>(entityName: "Ejercicio")
//            request.returnsObjectsAsFaults = false
//            request.predicate = NSPredicate(format: "documentId = %@", ejercicio.documentId)
//            var result : [NSManagedObject] = []
//            do {
//                ejerAlmacenado = try context.fetch(request)[0] as! NSManagedObject
//                request = NSFetchRequest<NSFetchRequestResult>(entityName: "EjercicioParaSubir")
//                request.returnsObjectsAsFaults = false
//                request.predicate = NSPredicate(format: "ejercicio = %@", ejerAlmacenado)
//                do {
//                    result = try context.fetch(request) as! [NSManagedObject]
//                } catch {
//                    print("Failed to retrieve EjerciciosParaSubir array")
//                }
//                if result.count == 0 {
//                    let entity : NSEntityDescription?
//                    entity = NSEntityDescription.entity(forEntityName: "EjercicioParaSubir", in: context)
//                    let ejercicioParaSubir : NSManagedObject = NSManagedObject(entity: entity!, insertInto: context)
//                    ejercicioParaSubir.setValue(ejerAlmacenado, forKey: "ejercicio")
//                }
//            } catch {
//                print("Failed to retrieve exercise")
//            }
//
//            do {
//                try context.save()
//            } catch {
//                print("Failed saving to CoreData")
//            }
//        }
//    }
    
//    private func actualizarEjercicioLocal(_ ejercicio : EjercicioEntityDTO) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        var ejer : NSManagedObject
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Ejercicio")
//        request.returnsObjectsAsFaults = false
//        request.predicate = NSPredicate(format: "documentId = %@", ejercicio.documentId)
//        do {
//            ejer = try context.fetch(request)[0] as! NSManagedObject
//            ejer.setValue(ejercicio.completitud, forKey: "completitud")
//        } catch {
//            print("Failed")
//        }
//
//        do {
//            try context.save()
//        } catch {
//            print("Failed saving to CoreData")
//        }
//    }
//
 
    
    func entityToDTO(puntoEntity ejerAlmacenado : NSManagedObject) -> PuntoMapaDTO{
     
     
        return PuntoMapaDTO(mensaje: ejerAlmacenado.value(forKey: "mensaje") as! String, nombreSitio: ejerAlmacenado.value(forKey: "nombreSitio") as! String, latitud: ejerAlmacenado.value(forKey: "latitud") as! Double, longitud: ejerAlmacenado.value(forKey: "longitud") as! Double, documentId: ejerAlmacenado.value(forKey: "documentId") as! String)
    }
}
