//
//  myMapScreen.swift
//  FisioApp
//
//  Created by CHRISTIAN GUSTAVO CHAVARRO ESPEJO on 4/22/19.
//
import UIKit
import CoreLocation
import GoogleMaps
import UserNotifications



class mapScreen: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate{

    @IBOutlet var vistaMapa: UIView!
    
    
    var numeroTelefono = 3192684086
    
    
  var puntoNotificacion:PuntoMapaDTO=PuntoMapaDTO(mensaje: "Estas cerca a un centro de fisioterapia", nombreSitio: "Prueba", latitud: 4, longitud: 4, documentId: "Prueba")
    @IBAction func actionNotificacion(_ sender: AnyObject? = nil)
    {
        
       print("Entra a armarla")
        let content = UNMutableNotificationContent()
        content.title = puntoNotificacion.nombreSitio
        content.subtitle=puntoNotificacion.mensaje
      //  content.body="Prueba de esta cosa para cuerpo"
        content.badge=1
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        
        let request = UNNotificationRequest(identifier: "Tiempo completo", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
      
        
    }
    let locationManager = CLLocationManager ()
//    var mapView = GMSMapView()
//    var camera = GMSCameraPosition()
    
    
    var puntos=[PuntoMapaDTO]()
 //   var ubicacion:PuntoMapaDTO
    
    private func cargarPuntos() {
        DispatchQueue.main.async {
            let dao = PuntoMapaDAO()
            dao.getPuntosCentrosMedicos{
                puntos in
                self.puntos = puntos
              //  self.tableView.reloadData()
            }
        }
    }
    
   var preferedStatudBarStyle: UIStatusBarStyle{ return .lightContent}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      cargarPuntos()
        
      
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy=kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllo2, error in
            
        })
        
       
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.showCurrentLocationOnMap()
        self.locationManager.stopUpdatingLocation()
    }
    
//    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) -> Bool {
//
//        let phoneNumber =  "+573124269322"
//        let appURL = NSURL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
//        if UIApplication.shared.canOpenURL(appURL as URL) {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
//            }
//            else {
//                UIApplication.shared.openURL(appURL as URL)
//            }
//        }
//        else {
//            // Whatsapp is not installed
//        }
//
//        print("Hizo click en prueba de esto")
//        return true
//    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        let appURL = NSURL(string: "https://api.whatsapp.com/send?phone=\(numeroTelefono)")!
        if UIApplication.shared.canOpenURL(appURL as URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL as URL)
            }
        }
        else {
            // Whatsapp is not installed
        }
        
    //    print("Hizo click en prueba de esto")
       
    }
    
//   func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
//        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: 250, height: 50))
//        view.backgroundColor=UIColor.white
//        view.layer.cornerRadius=6
//
//        let label1 = UILabel(frame: CGRect.init(x: 8, y: 8, width: view.frame.size.width-16, height: 15))
//        label1.text = marker.title
//        view.addSubview(label1)
//
//        let label2 = UILabel(frame: CGRect.init(x: label1.frame.origin.x, y: label1.frame.origin.y+label1.frame.size.height+3, width: view.frame.size.width-16, height: 15))
//
//        label2.text = "Has clic aquí para contactarte"
//      label2.font = UIFont.systemFont(ofSize: 14, weight: .light)
//        view.addSubview(label1)
//    view.addSubview(label2)
//
//        return view
//
//
//    }
    
    
    
    func showCurrentLocationOnMap(){
        let camera = GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!, zoom: 14)
        
        
        let mapView=GMSMapView.map(withFrame: CGRect.init(x: 0, y: 0, width: self.vistaMapa.frame.size.width, height: self.vistaMapa.frame.size.height), camera: camera)
        
        mapView.delegate=self
       
        
        
        
        mapView.settings.myLocationButton=true
        mapView.settings.scrollGestures=true
        mapView.settings.zoomGestures=true
        mapView.isMyLocationEnabled=true
        
        let marker = GMSMarker()
        marker.position=camera.target
        marker.snippet="Localización actual del usuario"
     //   marker.appearAnimation=
       
//        let positionPrueba=CLLocationCoordinate2D(latitude: 4.63392, longitude: -74.04303999)
//        let marker1=GMSMarker(position: positionPrueba)
//        marker1.title="Punto de prueba en esta cosa, el quemado"
//        marker1.map=mapView
        
    //    print("Va a dibujar los puntos")
        
        for punto in puntos
        { //  print("Entra al if de puntos de dibujo")
            let positionPrueba1=CLLocationCoordinate2D(latitude: punto.latitud, longitude: punto.longitud)
            let marker1=GMSMarker(position: positionPrueba1)
          // let x = String(punto.numeroContacto)
            marker1.snippet = punto.mensaje
            marker1.title=punto.nombreSitio
            
      //  marker1.debugDescription=String(punto.numeroContacto)
            
            
            
          //  print(String(marker1.title?))
            marker1.map=mapView
        }
        
        do{
            print("Entra al if")
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json"){
                print(styleURL.absoluteString)
                  mapView.mapStyle=try GMSMapStyle(contentsOfFileURL: styleURL)
            }
            else{
                NSLog("No se logro")
            }
          
        }
        catch{
         //   print("Fallo al colorear el mapa")
        }
        
        calcularDistancia()
        
        marker.map=mapView
        
       
        
        self.vistaMapa.addSubview(mapView)
        
    }
    
    
    
   
    
    func calcularDistancia()
    {
        let Punto1 = CLLocation(latitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!)
        for punto in puntos
        {
            
            let Punto2 = CLLocation(latitude: punto.latitud, longitude: punto.longitud)
            
            let DistanciaEnMetros = Punto1.distance(from: Punto2)
            
            print(String(DistanciaEnMetros))
            if(DistanciaEnMetros<70)
            {
             print("Entra al if de la notificacion supuesta")
                puntoNotificacion.nombreSitio=punto.nombreSitio
                actionNotificacion()
                
            }
        }
    }
    
}
