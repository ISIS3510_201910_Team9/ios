//
//  PuntoMapaDTO.swift
//  FisioApp
//
//  Created by Christian Chavarro Espejo on 4/23/19.
//


import UIKit

class PuntoMapaDTO {

    var latitud: Double
    var longitud: Double
    var mensaje: String
    var nombreSitio: String
    var documentId : String
    var numeroContacto:Int
    
    init( mensaje : String, nombreSitio : String, latitud : Double, longitud: Double, documentId: String){
    
        self.latitud=latitud
        self.longitud=longitud
        self.mensaje=mensaje
        self.nombreSitio=nombreSitio
        self.documentId = documentId
        self.numeroContacto = 3192684086
        
        print("Se creo el punto" + nombreSitio)
    }
    
//    init( mensaje : String, nombreSitio : String, latitud : Double, longitud: Double, documentId: String, numeroContacto: Int){
//
//        self.latitud=latitud
//        self.longitud=longitud
//        self.mensaje=mensaje
//        self.nombreSitio=nombreSitio
//        self.documentId = documentId
//        self.numeroContacto = numeroContacto
//    }
    
}
