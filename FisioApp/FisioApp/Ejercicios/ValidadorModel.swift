//
//  ValidadorModel.swift
//  FisioApp
//
//  Created by FERNANDO REYES BEJARANO on 3/26/19.
//

import CoreMotion

class ValidadorModel {
    
    //MARK: Parámetros
    
    var x1 : Double
    var y1 : Double
    var z1 : Double
    var x2 : Double
    var y2 : Double
    var z2 : Double
    
    var contar : Bool
    var conteo : Int
    
    var ejercicio : EjercicioEntityDTO?
    
    let RANGO = 0.2
    
    var ejercicioEntityDAO : EjercicioEntityDAO
    
    //MARK: Constructores
    
    init() {
        self.x1 = 0
        self.y1 = 0
        self.z1 = 0
        self.x2 = 0
        self.y2 = 0
        self.z2 = 0
        self.contar = false
        self.conteo = 0
        self.ejercicioEntityDAO = EjercicioEntityDAO()
    }
    
    //MARK: Métodos
    
    func setPuntoUno(x1: Double, y1: Double, z1: Double){
        self.x1 = x1
        self.y1 = y1
        self.z1 = z1
        print("Punto 1 \(self.x1) \(self.y1) \(self.z1)")
    }
    
    func setPuntoDos(x2: Double, y2: Double, z2: Double) -> Bool{
        let x = x2
        let y = y2
        let z = z2
        
        if !((x1-RANGO) < x && (y1-RANGO) < y && (z1-RANGO) < z && (x1+RANGO) > x && (y1+RANGO) > y && (z1+RANGO) > z) {
            self.x2 = x2
            self.y2 = y2
            self.z2 = z2
            print("Punto 2 \(self.x2) \(self.y2) \(self.z2)")
            return true
        } else {
            print("Punto inválido")
            print("Punto 2 inválido \(x2) \(y2) \(z2)")
            return false
        }
    }
    
    func termino () -> Bool{
//        return ejercicio!.repeticiones*ejercicio!.duracion <= conteo
        let termino = ejercicio!.repeticiones <= conteo
        if (termino) {
            ejercicio!.completitud = true
            ejercicioEntityDAO.actualizarEjercicio(ejercicio!)
        }
        return termino
    }
    
    func procesarCambioAccelerometro(datos : CMAcceleration?){
        let x = datos!.x
        let y = datos!.y
        let z = datos!.z
        
        if (x1-RANGO) < x && (y1-RANGO) < y && (z1-RANGO) < z && (x1+RANGO) > x && (y1+RANGO) > y && (z1+RANGO) > z && !contar{
            contar = true
            print("Contar")
        } else if (x2-RANGO) < x && (y2-RANGO) < y && (z2-RANGO) < z && (x2+RANGO) > x && (y2+RANGO) > y && (z2+RANGO) > z && contar{
            contar = false
            conteo+=1
            print("Uno más")
        } else {
            print("No entró " + String(contar))
            print("\(x) [\(x2-RANGO),\(x2+RANGO)] \(y) [\(y2-RANGO),\(y2+RANGO)] \(z) [\(z2-RANGO),\(z2+RANGO)] ")
        }
    }
    
    public func procesarCambioPodometro(_ conteo : Int){
        self.conteo = conteo
    }
    
}
