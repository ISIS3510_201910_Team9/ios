//
//  EjerciciosTableViewController.swift
//  FisioApp
//
//  Created by Fernando Reyes Bejarano on 3/21/19.
//  Copyright © 2019 Los Galacticos. All rights reserved.
//https://medium.com/@iboudhayan/anatomy-of-dispatch-queue-in-swift-part-1-basics-8c7a0a917056

import UIKit
import FirebaseFirestore
import Foundation
import GoogleSignIn

class EjerciciosTableViewController: UITableViewController, EjercicioTableViewCellDelegate {
    
    //MARK: Properties
    
    var ejercicios = [EjercicioEntityDTO]()
    
    let network = NetworkManager.sharedInstance
    
    //MARK:- View Controller life cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Load data.
        cargarEjercicios()
        showNoConexionMessage()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Remove listeners.
        network.reachability.whenUnreachable = nil
        network.reachability.whenReachable = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Check connectivity
        network.reachability.whenUnreachable = {
            [unowned self]
            reachability in
            let alert = UIAlertController(title: "Conexión perdida", message: "Se ha perdido la conexión. Se mostrarán los últimos ejercicios descargados.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Entendido", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }
        network.reachability.whenReachable = {
            [unowned self]
            reachability in
            let alert = UIAlertController(title: "Conexión recuperada", message: "Se ha recuperado la conexión. Se descargarán los ejercicios.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Entendido", style: .default, handler: nil))
            self.present(alert, animated: true)
            self.cargarEjercicios()
        }
        
        //Reload data
        tableView.reloadData()
        
        //Show current date in header.
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.locale = Locale(identifier: "es_CO")
        self.navigationItem.title = "Ejercicios del \(dateFormatter.string(from: Date()))"
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ejercicios.count
    }

    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EjerciciosTableViewCell", for: indexPath) as? EjercicioTableViewCell else {
            fatalError("No sé por qué cualquier ser humano usaría esto.")
        }
     
//      Configure the cell...
        let ejercicio = ejercicios[indexPath.row]
        
        //Valores
        cell.nombreEjercicioLabel.text = ejercicio.nombre
        cell.imagenImageView.image = ejercicio.imagen?.withRenderingMode(.alwaysTemplate)
        cell.imagenImageView.tintColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1)
        cell.duracionLabel.text = String(ejercicio.duracion)
        cell.repeticionesLabel.text = String(ejercicio.repeticiones)
        cell.circuitosLabel.text = String(ejercicio.cantidadRepeticiones)
        if ejercicio.completitud {
            cell.completitudImageView.image = UIImage.init(named: "check")?.withRenderingMode(.alwaysTemplate)
            cell.completitudImageView.tintColor = UIColor.init(red: 196/255, green: 231/255, blue: 212/255, alpha: 1)
        } else {
            cell.completitudImageView.image = nil
        }
        
        //Acciones.
        cell.delegate = self
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier != "salirEjer" {
            guard let ejercicioSleccionado = sender as? EjercicioEntityDTO else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            switch(segue.identifier ?? "") {
                
            case "validarFlexionSegue":
                guard let validadorConfigViewController = segue.destination as? ValidadorConfigViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                validadorConfigViewController.ejercicio = ejercicioSleccionado
                
            case "validarCaminataSegue":
                guard let validacionViewController = segue.destination as? ValidacionViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                validacionViewController.modelo = ValidadorModel()
                validacionViewController.modelo?.ejercicio = ejercicioSleccionado
            case "verVideoSegue":
                guard let detalleEjercicioViewController = segue.destination as? DetalleEjercicioViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                detalleEjercicioViewController.ejercicio = ejercicioSleccionado
                
            default:
                fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
            }
        }
        
        
        
    }
 
    
    //MARK:- Private Methods
    
    private func cargarEjercicios() {
        DispatchQueue.global(qos: .userInteractive).async {
            let dao = EjercicioEntityDAO()
            dao.getEjerciciosParaHoy{
                [weak self]
                ejercicios in
                DispatchQueue.main.async {
                    self?.ejercicios = ejercicios
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    func getEjercicioDeCelda(_ ejercicioTableViewCell: EjercicioTableViewCell) -> EjercicioEntityDTO{
        guard let indexPath = tableView.indexPath(for: ejercicioTableViewCell) else {
            fatalError("The selected cell is not being displayed by the table")
        }
        
        return ejercicios[indexPath.row]
    }
    
    //MARK:- EjercicioTableViewCellDelegate
    func validarButtonFuePresionado(_ ejercicioTableViewCell: EjercicioTableViewCell) {
        let ejercicioSeleccionado = getEjercicioDeCelda(ejercicioTableViewCell)
        switch ejercicioSeleccionado.tipoEjercicio {
            case EjercicioEntityDTO.TIPO_CAMINATA:
                performSegue(withIdentifier: "validarCaminataSegue", sender: ejercicioSeleccionado)
            default:
                performSegue(withIdentifier: "validarFlexionSegue", sender: ejercicioSeleccionado)
        }
        
    }
    
    func verVideoButtonFuePresionado(_ ejercicioTableViewCell: EjercicioTableViewCell) {
        let ejercicioSeleccionado = getEjercicioDeCelda(ejercicioTableViewCell)
        performSegue(withIdentifier: "verVideoSegue", sender: ejercicioSeleccionado)
    }
    func showNoConexionMessage(){
    
      
//        let url = URL(string: "http://api.waqi.info/feed/geo:4.7087315;-74.0306411/?token=b29a171aaeb8bd2cd4ea0bb6eeea755c331e184c")!
//
//        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
//            if error==nil
//            {
//                print(String(data: data!, encoding: .utf8)!)
//
//            }
//
//        }
//        task.resume()
      
        
        let alert = UIAlertController(title: "Calidad del aire hoy", message: "La calidad del aire hoy en Bogotá es óptima, puedes ejercitarte sin problemas", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        // messageShowed = true
    }
    
   
    
    
    

    //IBAction
    
    @IBAction func salir(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
        performSegue(withIdentifier: "salirEjer", sender: nil)
    }
    
    
}


