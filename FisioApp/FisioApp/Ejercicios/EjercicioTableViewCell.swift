//
//  EjercicioTableViewCell.swift
//  FisioApp
//
//  Created by Fernando Reyes Bejarano on 3/21/19.
//  Copyright © 2019 Los Galacticos. All rights reserved.
//

import UIKit

class EjercicioTableViewCell: UITableViewCell {
    
    //MARK:- Properties
    @IBOutlet weak var nombreEjercicioLabel: UILabel!
    @IBOutlet weak var imagenImageView: UIImageView!
    @IBOutlet weak var completitudImageView: UIImageView!
    @IBOutlet weak var duracionLabel: UILabel!
    @IBOutlet weak var repeticionesLabel: UILabel!
    @IBOutlet weak var circuitosLabel: UILabel!
    @IBOutlet weak var validarButton: UIButton!
    @IBOutlet weak var verVideoButton: UIButton!
    
    weak var delegate : EjercicioTableViewCellDelegate?
    
    //MARK:- UITableViewCell

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.validarButton.addTarget(self, action: #selector(validarButtonTapped(_:)), for: .touchUpInside)
        self.verVideoButton.addTarget(self, action: #selector(verVideoButtonTapped(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- IBActions

    @IBAction func validarButtonTapped(_ sender: UIButton){
        //https://fluffy.es/handling-button-tap-inside-uitableviewcell-without-using-tag/
        // ask the delegate (in most case, its the view controller) to
        // call the function 'subscribeButtonTappedFor' on itself.
        if let delegate = delegate {
            delegate.validarButtonFuePresionado(self)
        }
    }
    
    @IBAction func verVideoButtonTapped(_ sender: UIButton){
        //https://fluffy.es/handling-button-tap-inside-uitableviewcell-without-using-tag/
        // ask the delegate (in most case, its the view controller) to
        // call the function 'subscribeButtonTappedFor' on itself.
        if let delegate = delegate {
            delegate.verVideoButtonFuePresionado(self)
        }
    }
}
