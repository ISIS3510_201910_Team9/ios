//
//  DetalleEjercicioViewController.swift
//  FisioApp
//
//  Created by Fernando Reyes Bejarano on 3/24/19.
//  Copyright © 2019 Los Galacticos. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class DetalleEjercicioViewController: UIViewController {

    // MARK: Propiedades
    
    var ejercicio : EjercicioEntityDTO?
    
    @IBOutlet weak var videoEjercicioWKYTPlayerView: WKYTPlayerView!
    
    //MARK:- Ciclo de vida UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let ejercicio = ejercicio {
            videoEjercicioWKYTPlayerView.load(withVideoId: ejercicio.videoId)
        }
        
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//        super.prepare(for: segue, sender: sender)
//
//        
//    }
    
    
}

