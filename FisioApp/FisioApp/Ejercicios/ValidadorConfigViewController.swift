//
//  ValidadorViewController.swift
//  FisioApp
//
//  Created by FERNANDO REYES BEJARANO on 3/26/19.
//

import UIKit
import CoreMotion

class ValidadorConfigViewController: UIViewController {
    
    // MARK: Parametros
    
    /// CoreMotion manager instance we receive updates from.
    fileprivate let motionManager = CMMotionManager()
    
    var datosAccel : CMAcceleration?
    
    var modelo : ValidadorModel = ValidadorModel()
    
    var ejercicio : EjercicioEntityDTO?
    
    @IBOutlet weak var instruccionLabel: UILabel!
    
    @IBOutlet weak var mainButton: UIButton!
    
    @IBOutlet weak var siguienteButton: UIButton!
    
    var primerPunto : Bool = false
    var primerPuntoMod : Bool = false
    var segundoPunto : Bool = false
    var segundoPuntoMod : Bool = false
    
    let network = NetworkManager.sharedInstance
    
    //MARK:- Ciclo de vida del controlador.
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        startAccelerometerUpdates()
        siguienteButton.isEnabled = false
        primerPunto = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Remove listeners.
        network.reachability.whenUnreachable = nil
        network.reachability.whenReachable = nil
        
        // Stop sensor's observers.
        motionManager.stopAccelerometerUpdates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Check connectivity
        network.reachability.whenUnreachable = {
            [unowned self]
            reachability in
            let alert = UIAlertController(title: "Conexión perdida", message: "Se ha perdido la conexión. Se sincronizará la validación con el servidor cuando se recupere.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Entendido", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        super.prepare(for: segue, sender: sender)
        guard let validacionViewController = segue.destination as? ValidacionViewController else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        
        validacionViewController.modelo = self.modelo
        validacionViewController.modelo?.ejercicio = self.ejercicio
    }
 
    
    // MARK: - Configuring CoreMotion callbacks triggered for each sensor
    //https://github.com/navoshta/MotionSensorData
    
    /**
     *  Configure the raw accelerometer data callback.
     */
    fileprivate func startAccelerometerUpdates() {
        if motionManager.isAccelerometerAvailable && motionManager.isDeviceMotionAvailable{
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.deviceMotionUpdateInterval = 0.1
            motionManager.startAccelerometerUpdates(to: OperationQueue.main) {
                [unowned self]
                (accelerometerData, error) in
                self.process(acceleration: accelerometerData?.acceleration)
                self.log(error: error, forSensor: "Accelerometer")
            }
        }
    }
    
    /**
     Logs an error in a consistent format.
     
     - parameter error:  Error value.
     - parameter sensor: `DeviceSensor` that triggered the error.
     */
    fileprivate func log(error: Error?, forSensor sensor: String) {
        guard let error = error else { return }
        
        NSLog("Error reading data from \(sensor): \n \(error) \n")
    }
    
    /**
     Sets acceleration data values to a specified `DataTableSection`.
     
     - parameter acceleration: A `CMAcceleration` holding the values to set.
     - parameter section:      Section these values need to be applied to.
     */
    internal func process(acceleration: CMAcceleration?) {
//        modelo.procesarCambioAccelerometro(datos: acceleration)
        self.datosAccel = acceleration
    }
    
    @IBAction func mainButtonAction() {
        if let datosAccel = datosAccel {
            if primerPunto {
                modelo.setPuntoUno(x1: datosAccel.x, y1: datosAccel.y, z1: datosAccel.z)
                siguienteButton.isEnabled = true
                instruccionLabel.text = "Presiona el botón para actualizar el punto 1"
                primerPunto = false
                primerPuntoMod = true
            } else if primerPuntoMod {
                modelo.setPuntoUno(x1: datosAccel.x, y1: datosAccel.y, z1: datosAccel.z)
                siguienteButton.isEnabled = false
//                mainButton.setTitle("ESTABLECER PUNTO", for: .normal)
                instruccionLabel.text = "Ubica el teléfono en el punto 2 y presiona el botón para establecer punto"
                primerPuntoMod = false
                segundoPunto = true
            } else if segundoPunto {
                if !modelo.setPuntoDos(x2: datosAccel.x, y2: datosAccel.y, z2: datosAccel.z) {
                    let alert = UIAlertController(title: "Error", message: "El segundo punto no puede estar tan cerca del punto uno.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Entendido", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    
                } else {
                    siguienteButton.isEnabled = true
                    //                mainButton.setTitle("ACTUALIZAR PUNTO", for: .normal)
                    instruccionLabel.text = "Presiona el botón para actualizar el punto 2"
                    segundoPunto = false
                    segundoPuntoMod = true
                }
            } else if segundoPuntoMod {
                if !modelo.setPuntoDos(x2: datosAccel.x, y2: datosAccel.y, z2: datosAccel.z) {
                    let alert = UIAlertController(title: "Error", message: "El segundo punto no puede estar tan cerca del punto uno.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Entendido", style: .default, handler: nil))
                    self.present(alert, animated: true)
                } else {
                    segundoPuntoMod = false
                    performSegue(withIdentifier: "iniciarValidacionSegue", sender: nil)
                }
            } 
        }
    }
    
    @IBAction func siguienteButtonAction(_ sender: Any) {
        if primerPuntoMod {
            siguienteButton.isEnabled = false
//            mainButton.setTitle("ESTABLECER PUNTO", for: .normal)
            instruccionLabel.text = "Ubica el teléfono en el punto 2 y presiona el botón para establecer punto"
            primerPuntoMod = false
            segundoPunto = true
        } else if segundoPuntoMod {            
            performSegue(withIdentifier: "iniciarValidacionSegue", sender: nil)
        } else {
        }
    }
}
