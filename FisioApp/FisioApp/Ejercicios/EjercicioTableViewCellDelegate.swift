//
//  EjercicioTableViewCellDelegate.swift
//  FisioApp
//
//  Created by Fernando Reyes Bejarano on 4/21/19.
//

import Foundation

protocol EjercicioTableViewCellDelegate : AnyObject {
    func validarButtonFuePresionado(_ ejercicioTableViewCell : EjercicioTableViewCell)
    func verVideoButtonFuePresionado(_ ejercicioTableViewCell : EjercicioTableViewCell)
}
