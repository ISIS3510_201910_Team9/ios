//
//  EjercicioEntityDAO.swift
//  FisioApp
//
//  Created by Fernando Reyes Bejarano on 4/14/19.
//

import Foundation
import FirebaseFirestore
import CoreData

class EjercicioEntityDAO {
    
    //MARK:- Atributos.
    
    let db : Firestore = Firestore.firestore()
    
    //MARK:- Constructores
    
    init(){
        actualizarFirebase()
    }
    
    //MARK:- Funciones.
    
    func getEjerciciosParaHoy(completed: @escaping ([EjercicioEntityDTO]) -> Void) -> Void  {
        NetworkManager.isReachable {
            _ in
            self.getEjerciciosFirestore(completed: completed)
        }
        NetworkManager.isUnreachable {
            _ in
            self.getEjerciciosLocal(completed: completed)
        }
    }
    
    private func getEjerciciosFirestore(completed: @escaping ([EjercicioEntityDTO]) -> Void) {
        //        let image1 : UIImage? = UIImage(named: "noun_squatting_77784")
        //        let ejer1 : EjercicioListaEntity = EjercicioListaEntity(nombre: "Sentadillas",
        //                                                                tipoEjercicio: "Sentadillas",
        //                                                                imagen: image1)
        //        let ejer2 : EjercicioListaEntity = EjercicioListaEntity(nombre: "Sentadillas",
        //                                                                tipoEjercicio: "Sentadillas",
        //                                                                imagen: image1)
        //        let ejer3 : EjercicioListaEntity = EjercicioListaEntity(nombre: "Sentadillas",
        //                                                                tipoEjercicio: "Sentadillas",
        //                                                                imagen: image1)
        //        ejercicios += [ejer1, ejer2, ejer3]
        db.collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").getDocuments {
            (snapshot, err) in
            var ejercicios : [EjercicioEntityDTO] = []
            var value : NSDictionary
            var nombre : String
            var tipo : String
            var completitud : Bool
            var creacion : Date
            var descripcion : String
            var duracion : Int
            var repeticiones : Int
            var cantidadRepeticiones : Int
            var videoId : String
            var documentId : String
            var imagen : UIImage?
            for document in snapshot!.documents {
                value = document.data() as NSDictionary
                
                nombre = value["nombre"] as? String ?? "Queso"
                tipo = value["tipo"] as? String ?? "Galletas"
                completitud = value["completitud"] as? Bool ?? false
                creacion = value["creacion"] as? Date ?? Date()
                descripcion = value["descripcion"] as? String ?? ""
                duracion = value["duracion"] as? Int ?? 1
                repeticiones = value["repeticiones"] as? Int ?? 1
                cantidadRepeticiones = value["cantidadRepeticiones"] as? Int ?? 1
                videoId = value["video"] as? String ?? "v=0Bcq3NXomaQ"
                videoId = videoId.components(separatedBy: "v=")[1]
                documentId = document.documentID
                
                if let imagenTest = UIImage(named: nombre) {
                    imagen = imagenTest
                } else {
                    imagen = UIImage(named: "Sentadillas")
                }
                ejercicios.append(EjercicioEntityDTO(nombre: nombre, tipoEjercicio: tipo, imagen: imagen,
                                                     completitud: completitud, creacion: creacion, descripcion: descripcion, duracion: duracion, repeticiones: repeticiones, videoId: videoId, documentId : documentId, cantidadRepeticiones : cantidadRepeticiones
                ))
            }
            completed(ejercicios)
            self.guardarEjerciciosLocal(ejercicios: ejercicios)
        }
    }
    
    private func guardarEjerciciosLocal(ejercicios : [EjercicioEntityDTO]){
        // https://medium.com/xcblog/core-data-with-swift-4-for-beginners-1fc067cca707
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            DispatchQueue.global(qos: .background).async {
                let context = appDelegate.persistentContainer.viewContext
                var entity : NSEntityDescription?
                var newEjer : NSManagedObject
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Ejercicio")
                request.returnsObjectsAsFaults = false
                var result : [Any]
                for ejer in ejercicios {
                    request.predicate = NSPredicate(format: "documentId = %@", ejer.documentId)
                    do {
                        result = try context.fetch(request)
                        if result.count == 0 {
                            entity = NSEntityDescription.entity(forEntityName: "Ejercicio", in: context)
                            newEjer = NSManagedObject(entity: entity!, insertInto: context)
                            newEjer.setValue(ejer.nombre, forKey: "nombre")
                            newEjer.setValue(ejer.tipoEjercicio, forKey: "tipoEjercicio")
                            newEjer.setValue(ejer.completitud, forKey: "completitud")
                            newEjer.setValue(ejer.creacion, forKey: "creacion")
                            newEjer.setValue(ejer.descripcion, forKey: "descripcion")
                            newEjer.setValue(ejer.duracion, forKey: "duracion")
                            newEjer.setValue(ejer.repeticiones, forKey: "repeticiones")
                            newEjer.setValue(ejer.videoId, forKey: "videoId")
                            newEjer.setValue(ejer.documentId, forKey: "documentId")
                            newEjer.setValue(ejer.cantidadRepeticiones, forKey: "cantidadRepeticiones")
                        }
                    } catch {
                        print("Failed")
                    }
                }
                do {
                    try context.save()
                } catch {
                    print("Failed saving to CoreData")
                }
            }
        }
    }
    
    private func getEjerciciosLocal(completed: @escaping ([EjercicioEntityDTO]) -> Void) {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            DispatchQueue.global(qos: .userInteractive).async {
                let context = appDelegate.persistentContainer.viewContext
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Ejercicio")
                request.returnsObjectsAsFaults = false
                var result : [NSManagedObject]
                var ejercicios : [EjercicioEntityDTO] = []
                
                do {
                    result = try context.fetch(request) as! [NSManagedObject]
                    for ejerAlmacenado in result {
                        ejercicios.append(self.entityToDTO(ejercicioEntity: ejerAlmacenado))
                    }
                    ejercicios.sort{
                        (ejer1, ejer2)
                        in
                        return ejer1.creacion < ejer2.creacion
                    }
                    completed(ejercicios)
                } catch {                    
                    print("Failed")
                }
            }
        }
    }
    
    public func actualizarEjercicio(_ ejercicio : EjercicioEntityDTO){
        actualizarEjercicioFirestore(ejercicio)
        actualizarEjercicioLocal(ejercicio)
    }
    
    private func actualizarEjercicioFirestore(_ ejercicio : EjercicioEntityDTO){
        NetworkManager.isReachable{
            _ in
            self.db.collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(ejercicio.documentId).setData([
                "completitud" : ejercicio.completitud
            ], merge: true) {
                err
                in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
        }
        NetworkManager.isUnreachable{
            _ in
            // https://medium.com/xcblog/core-data-with-swift-4-for-beginners-1fc067cca707
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            var ejerAlmacenado : NSManagedObject = NSManagedObject()
            var request = NSFetchRequest<NSFetchRequestResult>(entityName: "Ejercicio")
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "documentId = %@", ejercicio.documentId)
            var result : [NSManagedObject] = []
            do {
                ejerAlmacenado = try context.fetch(request)[0] as! NSManagedObject
                request = NSFetchRequest<NSFetchRequestResult>(entityName: "EjercicioParaSubir")
                request.returnsObjectsAsFaults = false
                request.predicate = NSPredicate(format: "ejercicio = %@", ejerAlmacenado)
                do {
                    result = try context.fetch(request) as! [NSManagedObject]
                } catch {
                    print("Failed to retrieve EjerciciosParaSubir array")
                }
                if result.count == 0 {
                    let entity : NSEntityDescription?
                    entity = NSEntityDescription.entity(forEntityName: "EjercicioParaSubir", in: context)
                    let ejercicioParaSubir : NSManagedObject = NSManagedObject(entity: entity!, insertInto: context)
                    ejercicioParaSubir.setValue(ejerAlmacenado, forKey: "ejercicio")
                }
            } catch {
                print("Failed to retrieve exercise")
            }
            
            do {
                try context.save()
            } catch {
                print("Failed saving to CoreData")
            }
        }
    }
    
    private func actualizarEjercicioLocal(_ ejercicio : EjercicioEntityDTO) {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            DispatchQueue.global(qos: .background).async {
                let context = appDelegate.persistentContainer.viewContext
                var ejer : NSManagedObject
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Ejercicio")
                request.returnsObjectsAsFaults = false
                request.predicate = NSPredicate(format: "documentId = %@", ejercicio.documentId)
                do {
                    ejer = try context.fetch(request)[0] as! NSManagedObject
                    ejer.setValue(ejercicio.completitud, forKey: "completitud")
                } catch {
                    print("Failed")
                }
                
                do {
                    try context.save()
                    print("Ejercicio Local Actualizado Luego de Validación")
                } catch {
                    print("Failed saving to CoreData")
                }
            }
        }
        
    }
    
    func actualizarFirebase(){
        NetworkManager.isReachable {
            _ in
            DispatchQueue.main.async {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                DispatchQueue.global(qos: .background).async {
                    let context = appDelegate.persistentContainer.viewContext
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EjercicioParaSubir")
                    request.returnsObjectsAsFaults = false
                    var result : [NSManagedObject]
                    
                    do {
                        result = try context.fetch(request) as! [NSManagedObject]
                        var ejercicioAlmacenado : NSManagedObject
                        for ejercicioParaSubir in result  {
                            ejercicioAlmacenado = ejercicioParaSubir.value(forKey: "ejercicio") as! NSManagedObject
                            context.delete(ejercicioParaSubir)
                            self.actualizarEjercicioFirestore(self.entityToDTO(ejercicioEntity: ejercicioAlmacenado))
                        }
                    } catch {
                        
                        print("Failed")
                    }
                }
            }
        }
    }
    
    func entityToDTO(ejercicioEntity ejerAlmacenado : NSManagedObject) -> EjercicioEntityDTO{
        var imagenEjer : UIImage?
        if let imagen = UIImage(named: (ejerAlmacenado.value(forKey: "nombre") as! String)) {
            imagenEjer = imagen
            print("La imagen existe: \(ejerAlmacenado.value(forKey: "nombre") as! String)")
        } else {
            imagenEjer = UIImage(named: "Sentadillas")
            print("La imagen no existe: \(ejerAlmacenado.value(forKey: "nombre") as! String)")
        }
        return EjercicioEntityDTO(nombre: ejerAlmacenado.value(forKey: "nombre") as! String, tipoEjercicio: ejerAlmacenado.value(forKey: "tipoEjercicio") as! String, imagen: imagenEjer,
                                  completitud: ejerAlmacenado.value(forKey: "completitud") as! Bool, creacion: ejerAlmacenado.value(forKey: "creacion") as! Date, descripcion: ejerAlmacenado.value(forKey: "descripcion") as! String, duracion: ejerAlmacenado.value(forKey: "duracion") as! Int, repeticiones: ejerAlmacenado.value(forKey: "repeticiones") as! Int, videoId: ejerAlmacenado.value(forKey: "videoId") as! String, documentId : ejerAlmacenado.value(forKey: "documentId") as! String, cantidadRepeticiones : ejerAlmacenado.value(forKey: "cantidadRepeticiones") as! Int)
    }
}
