//
//  EjercicioListaEntity.swift
//  FisioApp
//
//  Created by Fernando Reyes Bejarano on 3/21/19.
//  Copyright © 2019 Los Galacticos. All rights reserved.
//
import UIKit

class EjercicioEntityDTO {
    
    //MARK:- Constantes
    
    static let TIPO_CAMINATA : String = "Caminata"
    
    //MARK:- Propiedades
    
    var nombre : String
    var tipoEjercicio : String
    var imagen : UIImage?
    var completitud : Bool
    var creacion : Date
    var descripcion : String
    var duracion : Int
    var repeticiones : Int
    var videoId : String
    var documentId : String
    var cantidadRepeticiones : Int
    
    
    
    //MARK:- Constructores
    
    init(nombre : String, tipoEjercicio : String, imagen : UIImage?, completitud: Bool, creacion : Date, descripcion : String, duracion : Int, repeticiones : Int, videoId : String, documentId : String, cantidadRepeticiones : Int){
        self.nombre = nombre
        self.tipoEjercicio = tipoEjercicio
        self.imagen = imagen
        self.completitud = completitud
        self.creacion = creacion
        self.descripcion = descripcion
        self.duracion = duracion                                    
        self.repeticiones = repeticiones
        self.videoId = videoId
        self.documentId = documentId
        self.cantidadRepeticiones = cantidadRepeticiones
    }
    
}
