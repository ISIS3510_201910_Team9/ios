//
//  ValidacionViewController.swift
//  FisioApp
//
//  Created by FERNANDO REYES BEJARANO on 3/26/19.
//

import UIKit
import CoreMotion

class ValidacionViewController: UIViewController {
    
    //MARK:- Parámetros
    
    var modelo : ValidadorModel?
    
    @IBOutlet weak var labelConteo: UILabel!
    @IBOutlet weak var completoLabel: UILabel!
    @IBOutlet weak var instruccionLabel: UILabel!
    
    /// CoreMotion manager instance we receive updates from.
    fileprivate let motionManager = CMMotionManager()
    
    fileprivate let pedometer = CMPedometer()
    
    var initialTouchPoint : CGPoint = CGPoint(x: 0,y: 0)
    
    //Para actualizar la interfaz. https://makeapppie.com/2017/02/14/introducing-core-motion-make-a-pedometer/
    var timer = Timer()
    let timerInterval = 0.1
    var labelInstruccionText : String?
    var labelConteoText : String?
    var labelCompletoBool : Bool?
    
    //MARK:- Ciclo de vida controlador
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        motionManager.deviceMotionUpdateInterval = 0.1
        startTimer()
        if modelo?.ejercicio?.tipoEjercicio == EjercicioEntityDTO.TIPO_CAMINATA {
            startPedometerUpdates()
        } else {
            startAccelerometerUpdates()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if motionManager.isAccelerometerActive {
            motionManager.stopAccelerometerUpdates()
        }
        pedometer.stopUpdates()
    }
    
    // MARK: - Configuring CoreMotion callbacks triggered for each sensor
    //https://github.com/navoshta/MotionSensorData
    
    /**
     *  Configure the raw accelerometer data callback.
     */
    fileprivate func startAccelerometerUpdates() {
        if motionManager.isAccelerometerAvailable && motionManager.isDeviceMotionAvailable{
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.startAccelerometerUpdates(to: OperationQueue.main) {
                [unowned self]
                (accelerometerData, error) in
                self.process(acceleration: accelerometerData?.acceleration)
                self.log(error: error, forSensor: "Accelerometer")
            }
            
        }
    }
    
    
    fileprivate func startPedometerUpdates() {
        self.labelInstruccionText = "Siga caminando"
        if CMPedometer.isStepCountingAvailable() {
            pedometer.startUpdates(from: Date()) {
                [unowned self]
                pedometerData, error in
                guard let pedometerData = pedometerData, error == nil else { return }
                self.log(error: error, forSensor: "Pedometer")
                DispatchQueue.main.async {
                    self.modelo!.procesarCambioPodometro(pedometerData.numberOfSteps.intValue)
                    if self.modelo!.termino() {
                        self.pedometer.stopUpdates()
                        self.labelCompletoBool = true
                        self.labelInstruccionText = nil
                    } else {
                        self.labelInstruccionText = "Siga caminando"
                    }
                    self.labelConteoText = String(self.modelo!.conteo)
                }
            }
        }
    }
    
    /**
     Logs an error in a consistent format.
     
     - parameter error:  Error value.
     - parameter sensor: `DeviceSensor` that triggered the error.
     */
    fileprivate func log(error: Error?, forSensor sensor: String) {
        guard let error = error else { return }
        
        NSLog("Error reading data from \(sensor): \n \(error) \n")
    }
    
    /**
     Sets acceleration data values to a specified `DataTableSection`.
     
     - parameter acceleration: A `CMAcceleration` holding the values to set.
     - parameter section:      Section these values need to be applied to.
     */
    internal func process(acceleration: CMAcceleration?) {
        //        modelo.procesarCambioAccelerometro(datos: acceleration)
        modelo?.procesarCambioAccelerometro(datos: acceleration)
        if modelo!.contar{
            labelInstruccionText = "Ir al punto 2"
        } else {
            labelInstruccionText = "Ir al punto 1"
        }
        if modelo!.termino() {
            motionManager.stopAccelerometerUpdates()
            labelCompletoBool = true
            labelInstruccionText = nil
        }
        labelConteoText = String(modelo!.conteo)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK:- IBActions
    
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    
    //MARK: - timer functions
    func startTimer(){
        if timer.isValid { timer.invalidate() }
        timer = Timer.scheduledTimer(timeInterval: timerInterval,target: self,selector: #selector(timerAction(timer:)) ,userInfo: nil,repeats: true)
    }
    
    func stopTimer(){
        timer.invalidate()
        displayData()
    }
    
    @objc func timerAction(timer:Timer){
        displayData()
    }
    
    func displayData(){
        //Number of repetitions
        if let labelConteoText = self.labelConteoText{
            labelConteo.text = labelConteoText
        }
        
        //Instrucciones
        if let labelInstruccionText = self.labelInstruccionText{
            instruccionLabel.text = labelInstruccionText
        } else {
            instruccionLabel.text = ""
        }
        
        //Completo
        if let labelCompletoBool = self.labelCompletoBool{
            if labelCompletoBool { completoLabel.text = "Completo" }
        }
        
        
    }
}
